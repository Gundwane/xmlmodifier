import { BASE_URL, API_VERSION } from '../shared/base.url';
import { LoopBackConfig, Labels, LabelsApi } from '../shared/lbsdk/index';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';

  //labels :Array<Labels> = [];

  constructor(private apiLabel:LabelsApi) {
    LoopBackConfig.setBaseURL(BASE_URL);          //Setea el url de la api
    LoopBackConfig.setApiVersion(API_VERSION);
    //this.getLabels();
  }

//METODO EJEMPLO TRAER LABELS DE LA API
/*
  getLabels(){
    this.apiLabel.find()
      .subscribe((labels:Array<Labels>) =>
    {
      this.labels = labels
    console.log(labels[0].value)}
  );}
  */
}
