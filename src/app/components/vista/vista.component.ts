import { Component, OnInit } from '@angular/core';
import { LoopBackConfig } from '../../../shared/lbsdk/lb.config';
import { BASE_URL, API_VERSION } from '../../../shared/base.url';
import { LabelsApi, Labels } from '../../../shared/lbsdk/index';


@Component({
  selector: 'app-vista',
  templateUrl: './vista.component.html'
})
export class VistaComponent{
  labels :Array<Labels> = [];

  title: string = "Lenguaje App";
  alertMsg: string = '';

  constructor(private apiLabel:LabelsApi) {
    LoopBackConfig.setBaseURL(BASE_URL);          //Setea el url de la api
    LoopBackConfig.setApiVersion(API_VERSION);
     this.getLabels();
  } 
  
  guardado(): void {
    this.alertMsg = '¡Guardado exitoso!';
  }

  closeAlert(): void {
    this.alertMsg = '';
  }

  //METODO EJEMPLO TRAER LABELS DE LA API

  getLabels(){
    this.apiLabel.find()
      .subscribe((labels:Array<Labels>) =>
    {
      this.labels = labels
      console.log(this.labels)
    }
  );}

  addRow(){
    let label :Array<Labels> = [];
    this.labels.push(label);
  }

  createOrEditLabel(label: Labels){
    console.log('New label: ', label);
    let newId;
    //if (!label.key) {console.log('NO KEY >:( '); return;}
    if (!label.value) {console.log('NO VALUE >:( '); return;}
    if (!label.idlabel) {
      /*newId = this.labels.length+1;
      label.idlabel = newId;*/
      label.key = 'lbldeprueba';
      label.lang = 'en';
      console.log('New label: ', label);
      this.apiLabel.patchOrCreate(label)
      .subscribe();
      return;
    }
    this.apiLabel.patchOrCreate(label)
    .subscribe();
  }

  deleteLabel(id){
    let index = this.labels.findIndex(labels => labels.idlabel === id);
    this.labels.splice(index, 1);
    this.apiLabel.deleteById(id)
    .subscribe();
  }

  editLabel(label){
    this.apiLabel.patchAttributes(label.idlabel, label)
    .subscribe();
  }
  
}
