import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { VistaComponent } from './components/vista/vista.component';
import { SDKBrowserModule } from '../shared/lbsdk/index';


@NgModule({
  declarations: [
    AppComponent,
    VistaComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule, 
    HttpClientModule,
    SDKBrowserModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
