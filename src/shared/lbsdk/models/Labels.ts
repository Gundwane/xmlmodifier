/* tslint:disable */

declare var Object: any;
export interface LabelsInterface {
  "idlabel"?: number;
  "key": string;
  "lang": string;
  "value": string;
}

export class Labels implements LabelsInterface {
  "idlabel": number;
  "key": string;
  "lang": string;
  "value": string;
  constructor(data?: LabelsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Labels`.
   */
  public static getModelName() {
    return "Labels";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Labels for dynamic purposes.
  **/
  public static factory(data: LabelsInterface): Labels{
    return new Labels(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Labels',
      plural: 'Labels',
      path: 'Labels',
      idName: 'idlabel',
      properties: {
        "idlabel": {
          name: 'idlabel',
          type: 'number'
        },
        "key": {
          name: 'key',
          type: 'string'
        },
        "lang": {
          name: 'lang',
          type: 'string'
        },
        "value": {
          name: 'value',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
